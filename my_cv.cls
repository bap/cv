\NeedsTeXFormat{LaTeX2e}       % The version of LaTeX we are using
\ProvidesClass{my_cv}[2011/04/01 My custom CV class]

\LoadClass[11pt]{article}            % We base our class on the article class
\pagestyle{empty}			   % Remove the page numbering
\RequirePackage{titlesec}      % Package to customize titles
\usepackage{color}
\definecolor{darkblue}{rgb}{0.0,0.0,0.3}
\definecolor{darkred}{rgb}{0.3,0.0,0.0}

\titleformat{\section}         % Customise the \section command 
  {\large\scshape\raggedright} % Make the \section headers large (\Large),
                               % small capitals (\scshape) and left aligned (\raggedright)
  {}{0em}                      % Can be used to give a prefix to all sections, like 'Section ...'
  {\textcolor{darkred}}                           % Can be used to insert code before the heading
  [\titlerule]                 % Inserts a horizontal line after the heading
  

\newcommand{\datedentry}[2]{
  #1 \hfill \textcolor{darkblue}{#2}\\
}

\newcommand{\lol}[2]{
 \begin{center}
 \textcolor{darkblue}{\Large{\textsc{#1}}}\\ #2
 \end{center}
}

\newcommand{\bb}[1]{
  \textcolor{darkblue}{\textbf{#1}}
}

\newcommand{\entry}[2]{
  \textbf{#1}: #2\\
}

\newcommand{\contact}[5]{
\Large
\textbf{\textsc{#1}}\\
\normalsize
  #2 \\ #3 \\ #4 \\ #5 \\ Permis B
}